#pragma hdrstop

#include <fstream>
// Block.cpp: implementation of the Block class.
//
//////////////////////////////////////////////////////////////////////

using namespace std;

#include "Block.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
double Block::m_fZero=0;

Block::Block() : m_fData(0)
{
	Init(0);
}

Block::Block(int iSize) : m_fData(0)
{
	Init(iSize);
}

Block::Block(const Block & src) : m_fData(0)
{
	Init(src.GetSize());
    CopyData(src);
}

Block::~Block()
{
	delete [] m_fData;
}

void Block::Init(int iSize)
{
    unitX[0] = 0;
    unitY[0] = 0;
	delete [] m_fData;
	m_fData=0;
	if( (m_iSize=iSize) !=0 )
	{
		m_fData=new double[m_iSize];
		memset(m_fData,0,sizeof(double)*m_iSize);
	}
	m_fLeft=0;
	m_fCalx=1.0;
	m_fPow=1.0;
	m_eDomain=dmTime;
	m_eArgType=atLinear;
	m_bDoCheckRange=true;
}

bool Block::CheckRange(bool bDoCheck)
{
	bool bOld=m_bDoCheckRange;
	m_bDoCheckRange=bDoCheck;
	return bOld;
}

Block & Block::Fill(double fVal, int iKd, int iKf)
{
	ExpandKdKf( iKd, iKf );
	for(int i=iKd; i<=iKf; ++i) m_fData[i] = fVal;
	return *this;
}

	//  �������� ���������� ������������ �� ����� - ����������
	//  �������� � ��� ���������
Block & Block::Center()
{
    Add(-Average());
    return *this;
}

Block & Block::CopyData(const Block & bSrc, int iKd, int iKf, int iKn )
{
	ExpandKdKf( iKd, iKf );
	assert( iKf-iKd+iKn<m_iSize && iKn>=0 && iKd<=iKf );
    memcpy( m_fData+iKn, bSrc.m_fData+iKd, sizeof(m_fData[0])*(iKf-iKd+1));
	return *this;
}

	// ����������
Block & Block::Add(const Block & bOper)
{
	assert(m_iSize==bOper.m_iSize);
	for(int i=0;i<m_iSize;++i)
    	m_fData[i]+=bOper.m_fData[i];
	return *this;
}

Block & Block::Sub(const Block & bOper)
{
	assert(m_iSize==bOper.m_iSize);
	for(int i=0;i<m_iSize;++i)
    	m_fData[i]-=bOper.m_fData[i];
	return *this;
}

Block & Block::Mul(const Block & bOper)
{
	assert(m_iSize==bOper.m_iSize);
    if(m_eDomain==dmComp && bOper.m_eDomain==dmComp) {
    	int sz=m_iSize/2;
    	double
        	*a = m_fData,
        	*b = a + sz;
    	const double
        	*a2 = bOper.m_fData,
        	*b2 = a2 + sz;
		for(int i=0; i<sz; ++i)
    	{
        	double
            	re = *a * *a2 - *b * *b2,
            	im = *a * *b2++ + *a2++ * *b;
            *a++ = re;
            *b++ = im;
	    }
    }
    else
	for(int i=0;i<m_iSize;++i)
    	m_fData[i]*=bOper.m_fData[i];
	return *this;
}

Block & Block::Dib(const Block & bOper)
{
	assert(m_iSize==bOper.m_iSize);
	for(int i=0;i<m_iSize;++i)
    {
	    double fDiv=bOper.m_fData[i];
        if(fDiv) m_fData[i] /= fDiv;
       	else m_fData[i]=0;
    }
	return *this;
}

Block & Block::Add(const double & fOper)
{
	for(int i=0;i<m_iSize;++i) m_fData[i]+=fOper;
	return *this;
}

Block & Block::Mul(const double & fOper)
{
	for(int i=0;i<m_iSize;++i) m_fData[i]*=fOper;
	return *this;
}

Block & Block::Sqrt(void)
{
	for(int i=0;i<m_iSize;++i)
	{
		double fValue=m_fData[i];
		m_fData[i]= (fValue<0) ? -sqrt(-fValue) : sqrt(fValue);
	}
	return *this;
}

Block & Block::LogX10(void)	// to dB
{
	double
    	*d=m_fData,
        *dd=d,
        zero_value=1e+38;
	bool zero_value_ready=false;
	for (int i=0, last=m_iSize; i<last; i++)
  	{
  		double s=*d;
		if (s>0) s=log10(s);
    	else if (zero_value_ready) s=zero_value;
	  		else /* ����� ����� ����������� ������������� �����, ���
	   			�������� ����� ����������� � �������� ��������� ���� �
	   			������������� ����� */
	   		{
	   			for (int ii=0; ii<last; ii++)
		     	{ 	// ���� ������ ������������ �������������� �����
		     		s=*dd++;
	    	 		if (s>0 && s<zero_value)
					{
						zero_value=s;
						zero_value_ready=true;
					}
	     		}  // ���� ������ ����������� �������������� �����
	   		if (!zero_value_ready) break; /* ���� ���� - ������
	     		��� ����� ����, ������ � ��� �� ������ */
	   		zero_value=log10( zero_value );
	   		s=zero_value;
	   	}
      	*d++=s*10;
    }
	return *this;
}

Block & Block::PowX10(void)	// from dB
{
  	int size=GetSize();
  	double *d1=m_fData;
	for (int index=0; index<size; index++, d1++) *d1 = pow( 10, *d1/10 );
	return *this;
}

void Block::ExpandKdKf(int & iKd, int & iKf) const
{
	int iLastCount=m_iSize-1;
	if (iKf<0)
		iKf=(m_eDomain != dmDensity) ? iLastCount : iLastCount>>1;
	assert( iKd<m_iSize && iKf<m_iSize || iKd<iKf );
}

int Block::log2(int N)
{
	int M;
	for (M=0; (1 << M) < N; ) M++;
	assert((1 << M) == N);
	return M;
}

Block & Block::Hamming(void)
{
	int i,half;
	double prev1,prev2,now1,now2,next1,next2, *dr, *di;
	assert(m_eDomain==dmComp);
// ������� ���� - ���������� �������
	half=m_iSize>>1;
	prev2=prev1=0;
	di=(dr=m_fData)+half;
	now1=*dr;
	now2=*di;
	for (i=0; i<half; i++)
	{
		if (i<half-1)
		{
			next1=dr[1];
			next2=di[1];
		}
			else next2=next1=0;

		*di++ =now1/2-(prev1+next1)/4;
		*dr++ =now2/2-(prev2+next2)/4;
		prev1=now1;
		now1=next1;
		prev2=now2;
		now2=next2;
	}
	return *this;
}

void Block::FindMax(RmaxType & rResult, int iKd, int iKf) const
{
	ExpandKdKf( iKd, iKf );
	double
		* fPtr=m_fData+iKd,
		fMax=*fPtr++,
		fMin=fMax;
	int iMx=iKd,
		iMn=iKd;
	for(int i=iKd+1;i<=iKf;++i,++fPtr)
	{
		double
			fValue= *fPtr;
 		if(fValue<fMin) { fMin=fValue; iMn=i; }
		else if(fValue>fMax) { fMax=fValue; iMx=i; }
	}
	rResult.fMax=fMax;
	rResult.fMin=fMin;
	rResult.iMax=iMx;
	rResult.iMin=iMn;
}

	//  ���������� ����� �������� � ����� ���������.
	//  kd, kf - ��������� � �������� ������� ���� ������������.
	//  ����� : �����.
double Block::Area( int iKd, int iKf ) const
{
	ExpandKdKf( iKd, iKf );
	double
    	*d=m_fData+iKd,
    	sum=0;
	for (int i=iKd; i<=iKf; i++, sum+=*d++) ;
	return sum;
}
	//  ���������� �������� ��������������� � ����� ����� blo.
	//  kd, kf - ��������� � �������� ������� ���� ������������.
	//  ����� : ������� ��������������.
double Block::Average( int iKd, int iKf ) const
{
	ExpandKdKf( iKd, iKf );
	double
    	*d=m_fData+iKd,
    	sum=0;
	for (int i=iKd; i<=iKf; i++, sum+=*d++) ;
	return sum/(iKf-iKd+1);
}

	//  ���������� ������������������� ���������� � ����� �����.
	//  kd, kf - ��������� � �������� ������� ���� ������������.
	//  ����� : �����������������e ���������e.
double Block::RMS( int iKd, int iKf ) const
{
	ExpandKdKf( iKd, iKf );
	double
    	*d=m_fData+iKd,
    	sum=0,
        mid=Average( iKd, iKf );
	// ������� ���������
	for (int i=iKd; i<=iKf; i++, d++)
	{
		double dif= *d-mid;
		sum+= (dif*dif);
	}
	return sqrt(sum/(iKf-iKd));
}
	// ��������� ����������������� ������ �����
	// p�������� ��������� �� ����� �������� ������.
Block & Block::Derivate()
{
	double der = 0;
	for(int i=0, n=m_iSize-1; i<n; ++i)
    {
    	double val = m_fData[i];
    	m_fData[i] = der;
        der = m_fData[i+1] - val;
    }
    return *this;
}
	// �������������� �������� �����
	// p�������� ��������� �� ����� �������� ������.
Block & Block::Integral()
{
	double s = 0;
	for(int i=0; i<m_iSize; ++i)
    {
    	double val = m_fData[i];
    	m_fData[i] = s += val;
    }
    return *this;
}

	//  �������� ������������ ������ � �����.
Block & Block::Linc()
{
int now=0,
	rght,
	lft=-1,
	last=GetSize()-1;
double
	deriv,
    rght_val,
    lft_val,
    *vals,
    *val_now=m_fData;

do {
  if (!*val_now)
   if (lft!=-1)
     { // ��� ���� ��������� ������� - ��� ����� ���������������
     vals=val_now; //  ���� ������ �������
     rght=now;
     do {
       rght++;
       vals++;
       rght_val=*vals;
       }
     while (rght<=last && rght_val==0);
     // �� ����� ����� ������� ������� - ����� �������� }
     if (rght>last) break;
     // ���� �����, ��������� �����
     deriv=(rght_val-lft_val)/(rght-lft);
     // ��������� ������ ��������� �������� �� �����, �������
     //  ��������� ���� ������������
     do {
       lft_val=lft_val+deriv;
       *val_now=lft_val;
       now++;
       val_now++;
       }
     while (now!=rght);
     }
       else
     { // �� ���� ��� ��������� ��������
     now++;
     val_now++;
     }
      else
   { // ���������� ��������� ������
   lft_val=*val_now;
   lft=now;
   now++;
   val_now++;
   }
  }
while (now<last);
return *this;
}

class __Hermith {
	int	numpo;
	long	divider, numpo3;
public:
	__Hermith(int n) { numpo3=3*(numpo=n); divider=(long)n*n*n; }
	double spline( int pos, double x1, double x2 )
	{
		return x1+(x2-x1)*(long)pos*pos*(numpo3-2*pos)/divider;
	}
};

	//  Hermith smoothing.
    // If closed == true, the first and last result valus
    // must be the same or close.
Block & Block::Hermith( int closed )
{
	int prev = - m_iSize - 1;
	int first = -1;
    double prevVal = 0;
    if(closed)
    {
    	prev = m_iSize - 1;
        while( m_fData[prev] == 0 )
        	if( prev-- == 0 ) return *this;
        prevVal = m_fData[prev];
        prev -= m_iSize;
    }

    for(int next=0; ; next = prev + 1)
    {
    	for( ; next < m_iSize; ++next)
        	if( next >= 0 && m_fData[next] != 0 )
            	break;

        double nextVal = 0;
        if( next >= m_iSize )
        {
        	if(!closed) break;
            else
            {
            	next = first + m_iSize;
                nextVal = m_fData[first];
	        	// closing part will be filled just now.
                // don't need to do it again.
                closed = false;
            }
        }
        else
        {
        	if(first<0)
            	first = next;	// used for closing loop
            nextVal = m_fData[next];
        }

        if( prev < -m_iSize )
        {
            // not closed: just the first point was found
	        prev = next;
    	    prevVal = nextVal;
        	continue;
        }

        // filling the zero-valued gap between prev and next
        __Hermith herm( next-prev );
        for(int i=prev+1; i<next; ++i)
        	if(i<0 || i>=m_iSize)
            	continue;
            else
            	m_fData[i] = herm.spline(i-prev, prevVal, nextVal);

        prev = next;
        prevVal = nextVal;
    }

    return *this;
}

	//  ���������� �������� �������� �����.
Block & Block::Fabs()
{
	for(int i=0; i<m_iSize; ++i)
    {
    	m_fData[i] = fabs( m_fData[i] );
    }
    return *this;
}
	//  ����������� �������� ����� ��������� max, min.
Block & Block::Limt( double max, double min )
{
	if(max < min)
    {
    	double t=min; min=max; max=t;
    }
    if(min==max)
    	return Fill( min );

	for(int i=0; i<m_iSize; ++i)
    {
    	double val = m_fData[i];
        if(val>max)
	    	m_fData[i] = max;
        else if(val<min)
	    	m_fData[i] = min;
    }

    return *this;
}

	//  ���������� ����������� ��������, ��������� � ����� this
	//  ����� ��������� kd � kf (������������).
	//  ��������� ������������� � ����� dest. ����� ������ �����������
	//  ������� ������������ ������� - ��������, dest.fill(); .
	//  ��� ��������� ���� �������� � ���������� �������� (����������������
	//  �������� ������� ����� dest) ������� ������������
	//  dest.calx() �  dest.left().
Block & Block::Hist(Block & dest, int iKd, int iKf)
{
	ExpandKdKf( iKd, iKf );
	double
		*source=m_fData,
        *destin=dest.m_fData,  // ��������� �� ������
		lowest=dest.Left(),
		step=dest.Calx(),
		position;
	int
		highest_index=dest.GetSize()-1;

	for (int index=iKd; index<=iKf; index++)
	{
		position=(source[index]-lowest) / step;
        int dest_index=(position<0) ? 0 :
			(position>highest_index) ? highest_index : position;
		destin[dest_index]++;
	}
	return dest;
}

Block & Block::Envelope( Block &bOper )
{
	for(int i=0; i<m_iSize; ++i)
		m_fData[i] = max( bOper.m_fData[i], m_fData[i] );
	return *this;
}

	//  ������� ����������� ������ � ������������� �����.
	//  ������� ������ ������������� ���� - � ����� this.
	//  ����� : module - ���� �������, phase - ���� ���.
Block & Block::ModPhase( Block & bModule, Block & bPhase )
{
	int nk=GetSize() / 2;
	if( bModule.GetSize() != nk ) bModule.Init( nk );
	if( bPhase.GetSize() != nk ) bPhase.Init( nk );
	double
    	*d1=m_fData,
        *d2=d1+nk,
      	*mo=bModule.m_fData,
      	*ph=bPhase.m_fData,
      	rad2deg=180/M_PI;
    for(int i=0;i<nk; i++, d1++, d2++ )
	{
    	double v1 = *d1;
    	double v2 = *d2;
  		*mo++ = sqrt( v1 * v1 + v2 * v2 );
  		*ph++ = (v1 || v2) ? atan2(-v2,v1) * rad2deg : 0;
  	}
    bModule.Left()=bPhase.Left()=m_fLeft;
    bModule.Calx()=bPhase.Calx()=m_fCalx;
	return *this;
}

	// ������� ����������� ������ �� ������������� �����.
	//  ���� : module - ���� �������, phase - ���� ���.
	//  ��������� ( ������ ������������� ���� ) ��������� � ���� this.
Block & Block::DeModPhase(Block & bModule, Block & bPhase)
{
	int nk=bModule.GetSize();
    assert( bPhase.GetSize() == nk );
	if( GetSize() != nk * 2 ) Init( nk * 2 );
	double
    	*d1=m_fData,
        *d2=d1+nk,
      	*mo=bModule.m_fData,
      	*ph=bPhase.m_fData,
      	deg2rad=M_PI/180;
	for (int i=0; i<nk; i++, d1++, d2++, ph++, mo++)
  	{
  		double argument=*ph * deg2rad;
		*d1= *mo * cos(argument);
		*d2= *mo * -sin(argument);
	}
	return *this;
}

    // ������� � �����, ������� ��������� � ����� flt
Block & Block::Convolution(Block & flt, progressFunc pfun, void * parg, int steps)
{
	int winSz=flt.GetSize(),
    	halfWin = winSz / 2,
    	sz=GetSize(),
        progStep = steps ? sz/steps : 1;
    if(!progStep) progStep=1;
    double
    	*val = new double[sz];
    memcpy(val,m_fData,sz*sizeof(double));
    flt.m_bDoCheckRange = false;
	for(int i=0;i<sz;++i)
    {
    	if(pfun && ((i % progStep)==0) )
        	if(!pfun(parg)) break;

    	double sum=0;
        for(int j=-halfWin;j<=halfWin;++j)
        {
        	int srcIndex=i+j;
         	if(srcIndex>=0 && srcIndex<sz)
            	sum+=val[srcIndex] * flt[halfWin-j];
        }
        m_fData[i]=sum;
    }
    flt.m_bDoCheckRange = true;
    delete [] val;
    return *this;
}

/////////////////////////////////////////////////////////
// �������� ��������
CBlockFile::CBlockFile(LPCSTR szFileName)
{
	ifstream cFile(szFileName, ios::in | ios::binary);
	int iSz=sizeof(m_cFileHeader);
	m_bIsValid=!!cFile.read((char*)&m_cFileHeader,iSz)
    	&& (cFile.gcount()==iSz)
		&& !strncmp(m_cFileHeader.ID,"BLOW",4);
}

	// ����� ��������� �� ����� ����, � ����� - �������,
	// � ������ ������� ������ � ������� �������� �������
bool Block::Load( LPCSTR szFileName, int iFileOffset, int iCntsNumber )
{
	CBlockFile cBlFile(szFileName);
	if( !cBlFile.IsValid() ) return false;
	if( iCntsNumber<0 )
		iCntsNumber=cBlFile.GetSize()-iFileOffset;
	if( iCntsNumber<=0 ) return false;
	Init( iCntsNumber );
	ifstream cFile( szFileName, ios::in | ios::binary );
	cFile.seekg(cBlFile.GetDataOffset()+iFileOffset*sizeof(m_fData[0]),ios::beg);
	cFile.read((char*)m_fData,sizeof(m_fData[0])*iCntsNumber);
	m_fLeft=cBlFile.GetStart();
	m_fCalx=cBlFile.GetStep();
	m_eArgType=cBlFile.GetArgumentType();
	m_eDomain=cBlFile.GetDomainType();
	return true;
}

	// ��� ������ �� ������� ����� ������ �����, ���������
	// � ���������, ������������� ��������������.
bool Block::Save( LPCSTR szFileName, int iFileOffset, int iCntsNumber )
{
	ofstream cFile(szFileName, ios::out | ios::binary );
	CBlockFileHeader cFileHeader;
	int iSz=sizeof(cFileHeader);
	strncpy(cFileHeader.ID,"BLOW",4);
	cFileHeader.iCntsNumber=m_iSize;
	cFileHeader.iDataOffset=iSz;
	cFileHeader.fStart=m_fLeft;
	cFileHeader.fStep=m_fCalx;
	cFileHeader.eArgType=m_eArgType;
	cFileHeader.eDomain=m_eDomain;

	cFile.write((char*)&cFileHeader,iSz);
	iSz=m_iSize*sizeof(m_fData[0]);
	cFile.write((char*)m_fData,iSz);
	return cFile.good();
}

/////////////////////////////////////////////////////////
// ���������� �����������
//void Block::Visu(TImage * Image, double mn, double mx, int divX, int divY )
//{
//	VisuRng(Image, 0, -1, mn, mx, divX, divY );
//}
//
//void Block::VisuRng(TImage * Image, int kd, int kf,
//	double mn, double mx, int divX, int divY )
//{
//    RECT r={ 0, 0, Image->Width, Image->Height };
//    TRect rect = TRect(r);
//    VisuRng( Image->Canvas, rect, kd, kf, mn, mx, divX, divY );
//}
//
//void Block::VisuRng(TCanvas * canv, TRect rect, int kd, int kf,
//	double mn, double mx, int divX, int divY )
//{
//	assert(m_iSize);
//    if(kf<0) kf = ( m_eDomain == dmDensity ? m_iSize/2 : m_iSize ) -1;
//	assert(kd < kf);
//
//    canv->Brush->Color=clWhite;
//    canv->FillRect( rect );
//    canv->Pen->Color = clBlack;
//    int hght = canv->TextHeight("0") + 4;
//    int Height = rect.Bottom - rect.Top;
//    int Width = rect.Right - rect.Left;
//    rect.Top += hght;
//    rect.Bottom -= hght;
//	canv->Brush->Color=clLtGray;
//    if(divX || divY)
//    {
//        canv->FrameRect( rect );
//        canv->Brush->Color=clWhite;
//    }
//    else
//        canv->FillRect( rect );
//    bool doMarks = true;
//    if( mn == mx )
//    {
//	    RmaxType rMax;
//		FindMax(rMax);
//        mn = rMax.fMin;
//        mx = rMax.fMax;
//        doMarks = false;
//    }
//
//    canv->Pen->Style = psDot;
//	canv->Pen->Color = clDkGray;
//    for(int i=1, h=Height-2*hght; i<divY; ++i)
//    {
//        int y = (int)((float)h/divY*i) + rect.Top;
//    	canv->MoveTo(rect.Left,y);
//        canv->LineTo(rect.Right,y);
//    }
//
//    for(int i=1; i<divX; ++i)
//    {
//        int x = (int)((float)Width/divX*i) + rect.Left;
//    	canv->MoveTo(x,rect.Top);
//        canv->LineTo(x,rect.Bottom);
//    }
//
//    canv->Pen->Style = psSolid;
//    if(mx!=mn)
//    {
//    	float
//   	    	scalex=(float)Width/(kf-kd),
//    	    scaley=(float)(Height-2*hght)/(mx-mn);
//        bool
//        	bPts = scalex > 3;
//
//        // ���� ������� ������� �����, �������� ������ �����
//        if(mx * mn < 0)
//        {
//    		canv->Pen->Color = clDkGray;
//        	if(!doMarks)
//            {
//        		mx = max(fabs(mx),fabs(mn));
//	            mn = -mx;
//    		    scaley=(float)(Height-2*hght)/(mx-mn);
//            }
//        	int Y=(int)(mx * scaley) + rect.Top;
//		    canv->MoveTo(rect.Left,Y);
//            canv->LineTo(rect.Right,Y);
//        }
//
//	    canv->Pen->Color = clBlack;
//        if((kf-kd+1) < Width)
//	        for(int i=kd;i<=kf;++i)
//    	    {
//     		    int X=i*scalex + rect.Left,
//        		    Y=(mx-m_fData[i])*scaley + rect.Top;
//    	    	if(i==kd) canv->MoveTo(X,Y); else canv->LineTo(X,Y);
//                if(bPts)
//                	canv->Ellipse(X-2,Y-2,X+2,Y+2);
//        	}
//   	    else
//	        for(int i=0, n=0;i<Width;++i)
//        	{
//         		int X=(i+1)/scalex+0.5;
//                if(X>kf) X=kf;
//		        RmaxType rLocalMax;
//    			FindMax( rLocalMax, n, X );
//                n=X;
//                int Y1=(mx-rLocalMax.fMax)*scaley + rect.Top,
//                	Y2=(mx-rLocalMax.fMin)*scaley + rect.Top;
//                if(Y1==Y2) Y2++;
//    	    	canv->MoveTo(i+rect.Left,Y1);
//                canv->LineTo(i+rect.Left,Y2);
//    	    }
//    }
//    canv->Brush->Color=clWhite;
//    rect.Top -= hght;
//    rect.Bottom = rect.Top + hght;
//    canv->FillRect( rect );
//    rect.Top=( rect.Bottom = rect.Top+Height ) - hght+1;
//    canv->FillRect( rect );
//    rect.Top = rect.Bottom - Height;
//    String unit(GetUnitY());
////    canv->TextOut(5,2,FloatToStrF(rMax.fMax,ffGeneral,8,6));
////    canv->TextOut(5,Image->Height-hght+2,FloatToStrF(rMax.fMin,ffGeneral,8,6));
//    if(doMarks)
//    {
//	    canv->TextOut(5+rect.Left,rect.Top+2,FloatToStr(mx) + unit );
//    	canv->TextOut(5+rect.Left,rect.Bottom-hght+2,FloatToStr(mn) + unit );
//    }
//}
/////////////////////////////////////////////////////////
// CFilter
double freq_response(double *A1,double *A2, int M_1,
	double BZERO, double f)
  /* ���祭�� ���⭮� �ࠪ���⨪� 䨫���,
    �����樥��� ���ண� ����� � ��६����� A1[], A2[], BZERO,
    ��� ����� f, ��ࠦ����� � ����� ����� ����⨧�樨 */
  {  /* freq_response */
  double A,AR,AI,S1,C1,S2,C2,ABSA,ANM,FD;
  int j;
  FD=2*M_PI*f;
  S1=sin(FD);
  C1=cos(FD);
  A=2*FD;
  S2=sin(A);
  C2=cos(A);
  ABSA=1;
  ANM=BZERO*BZERO;
  for (j=0; j<M_1; ++j)
    {
    AR=1+A1[j]*C1+A2[j]*C2;
    AI= -A1[j]*S1-A2[j]*S2;
    ABSA=ANM*ABSA/(AR*AR+AI*AI);
    }
  return ABSA;
}   /* freq_response */

void CFilter::bandPass( double FC, double BW, int Power )
{
  double A,B,C,D,E,G,H,FACT,SECTOR,ANG,ANG2,WEDGE,FREQ,CC,SS,HTRAN;
  int i;
  assert( Power<= 2*Max_Filter_Power );
  Half_Filter_Power=Power / 2;
  FACT=2*M_PI*BW;
  ANG2=2*M_PI*FC;
  CC=cos(ANG2)*cos(FACT);
  SS=sin(ANG2)*sin(FACT);
  FREQ=atan(sqrt(1-CC*CC)/CC)/(2*M_PI);
  HTRAN=0;
  SECTOR=M_PI/Half_Filter_Power;
  WEDGE=SECTOR/2;
  for (i=0; i<Half_Filter_Power; ++i)
    {
    ANG=i*SECTOR+WEDGE;
    A=SS*cos(ANG)+CC;
    B=SS*sin(ANG);
    C=1-(A*A+B*B);
    D=0.5*(-C+sqrt(C*C+4*B*B));
    E=sqrt(D+1)+sqrt(D);
    if (D==0 || E==0)
      assert( 0 );
    G=2*sqrt(1-B*B/D)/E;
    if (A<0) G=-G;
    H=-1/(E*E);
    A1[i]=-G;
    A2[i]=-H;
    HTRAN=HTRAN+log(freq_response(A1+i,A2+i,1,1,FREQ));
    }
  BZERO=exp(-HTRAN/(Half_Filter_Power*2));
}

double CFilter::filtrate( double val )
{
	for (int filt_step=0; filt_step< Half_Filter_Power; ++filt_step)
    	val = m_zv[filt_step].filt( val );
    return val;
}

void CFilter::init()
{
    for (int filt_step=0; filt_step< Half_Filter_Power; ++filt_step)
    {
    	m_zv[filt_step].init();
	    m_zv[filt_step].coeff(A1[filt_step],A2[filt_step],BZERO);
    }
}
/////////////////////////////////////////////////////////
// CFilter
void CBlockFilter::filtrate( Block & blo, progressFunc pfun, void * parg, int steps )
{
	init();

    int NK = blo.GetSize(),
        progStep = steps ? NK/steps : 1;
    if(!progStep) progStep=1;
    for (int i=0; i<NK; ++i)
    {
    	if(pfun && ((i % progStep)==0) )
        	if(!pfun(parg)) break;

        blo[i] = CFilter::filtrate( blo[i] );
    }
}

