#include "ofMain.h"
#include "testApp.h"
#include "ofAppGlutWindow.h"

//========================================================================
int main(int argc, char **argv){

    const char *imgf = 0;// "C:\\Users\\Victor\\Desktop\\IMG_2790.JPG";
    if (argc > 1) {
        imgf = argv[1];
    }
    if (imgf) {
        ofxIniSettings ini;
        ini.load("config.ini");
        float lowFreqA = ini.get("lowA", 40);
        float lowFreqB = ini.get("lowB", 400);
        float hiFreqA = ini.get("highA", 400);
        float hiFreqB = ini.get("highB", 14000);
        int outline = ini.get("outline", 1);


        cout << "arg: " << imgf << " "
            << lowFreqB << "..." << hiFreqB << endl;
        ofImage img;
        if (img.loadImage(imgf)) {
            imgPlayer p;
            p.convert(img, "snd.wav", lowFreqB, hiFreqB);

            ofSoundPlayer player;
            player.setLoop(false);
            player.setSpeed(1.0);
            player.setVolume(1.0);
            player.loadSound("snd.wav");
            player.play();
            imgPlayer::prepareBW(img, img, outline);
            img.saveImage("-b.jpg");
            puts("press enter to close.");
            char c;
            gets(&c);
//            sleep(1000);
        }
    } else {
        ofAppGlutWindow window;
        ofSetupOpenGL(&window, 1024,768, OF_FULLSCREEN);			// <-------- setup the GL context

        // this kicks off the running of my app
        // can be OF_WINDOW or OF_FULLSCREEN
        // pass in width and height too:
        ofRunApp( new testApp());
    }

}
