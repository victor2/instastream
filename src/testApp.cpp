#include <ofLog.h>
#include "testApp.h"

//--------------------------------------------------------------
void testApp::setup() {

    ini.load("config.ini");
    string log = ini.get("log", string("warning"));
    if (log == "notice") {
        ofLogLevel(OF_LOG_NOTICE);
    }

    ofLogToFile("ims.log", false);
    //ofLogLevel(OF_LOG_VERBOSE);
    ofLogNotice("Starting");

    // https://github.com/openframeworks/openFrameworks/issues/771
    ofDisableArbTex();

    period = ini.get("period", DOWNLOAD_PERIOD);
    email = ini.get("email",string("false")) == "true";
    int w = ini.get("width", 640);
    int h = ini.get("height", 480);
    int delay = ini.get("delay", 500);

    capturer.setDimensions(w, h, delay);
    capturer.setTiming(&period, &outline);
    string camids = ini.get("cameraid", string("0"));
    if (!capturer.init(camids, &provider))
    {
        cout << "FATAL : failed to init camera(s) " << camids << ". Closing application." << endl;
        ::exit(1);
    }
    resDir = ini.get("resdir", string("images\\res"));
    usedDir = ini.get("useddir", string("images\\used"));
    stockDir = ini.get("stockdir", string("images\\stock"));

    capturer.setPath(resDir);
    pop3writer.setApp(&provider, this, ini, resDir, &outline);
    provider.init(email, period, &stock, stockDir);

    nImages = DIR.listDir("images");
 	images = new ofImage[nImages];
    //you can now iterate through the files as you like
    for(int i = 0; i < nImages; i++){
        ofDirectory dir(DIR.getPath(i));
        if (dir.isDirectory()) continue;
        bool ok = images[i].loadImage(DIR.getPath(i));
        ofLog() << DIR.getPath(i) << " " << ok << endl;
    }
    deleted.loadImage("images\\deleted.jpg");
    currentImage = 0;

    int wd = ofGetScreenWidth();
    int ht = ofGetScreenHeight();
    screen.allocate(wd + 4, ht + 4, GL_RGB);
    ofSetWindowShape(wd/2, ht/2);
    ofSetWindowPosition(wd/4, ht/4);

	//for smooth animation, set vertical sync if we can
	ofSetVerticalSync(true);
	// also, frame rate:
	ofSetFrameRate(20);
    currentFrame = 0;
    updated = "init";

    stock = ini.get("stock", 0);

    imgIdx = 0;
    // transparency/fade transition
    alpha = ini.get("alpha", 0);
    shake = ini.get("shake", 0);

    // sound generator
    lowFreq = ini.get("lowB", 400);
    hiFreq = ini.get("highB", 14000);

    player.setLoop(true);
    player.setSpeed(1.0);
    player.setVolume(1.0);
    player.setMultiPlay(false);

    // colour control
    r = ini.get("red", 0);
    g = ini.get("green", 0);
    b = ini.get("blue", 0);

    movX = ini.get("movX", 0);
    movY = ini.get("movY", 0);

    spread = ini.get("spread", 3);
    outline = ini.get("outline", 1);

    // alpha shake r g b outline
    ctls[0].init(ini, "alpha", &alpha);
    ctls[1].init(ini, "shake", &shake);
    ctls[2].init(ini, "red", &r);
    ctls[3].init(ini, "green", &g);
    ctls[4].init(ini, "blue", &b);
    ctls[5].init(ini, "outline", &outline);

    // screen dumping
    dump = ini.get("dump", 0);
    dumpedAt = ofGetElapsedTimeMillis() + 10000; // start dumping a bit later

   	serial.enumerateDevices();
	int baud = ini.get("rate", 57600);
    if (serial.setup(ini.get("com", string("COM1")).c_str(), baud))
    {
        capturer.setSerial(&serial);
    }

    srand(ofGetElapsedTimeMillis());
}

//--------------------------------------------------------------
void testApp::update(){

    long now = ofGetElapsedTimeMillis();
    // screen dumping
    if (dump && (now > dumpedAt + dump) && screen.bAllocated())
    {
        ofImage tmp;
        tmp.grabScreen(0, 0, scrWd, scrHt);
        tmp.saveImage("images\\dump.jpg", OF_IMAGE_QUALITY_HIGH);
        cout << "after save" << endl;
        dumpedAt = now;
        cout << "dump.";
    }

    if (getNewImages())
    {
        t0 = now;
    }

    ofImage img;
    bool initial = nImages && (active.width == 0);
    if (initial) {
        img = images[currentImage];
    }

    if (updated.length() && (initial || img.loadImage(updated)))
    {
        fileMove(updated, usedDir);
        updated = "";
        images[currentImage] = img;
        active = getActiveArea(img.getWidth(), img.getHeight());
        currentFrame = 0;
        ofLogNotice() << "got a new image " << img.getWidth() << "x" << img.getHeight() << endl;
    }

    if (info) {
        info = false;
        fileMove(infoFile, usedDir);
    }

	if ((now - t0 > period * 4 / 5) &&
        (infoFile.length() != 0) && !info) {
        updated = infoFile;
        infoFile = "";
	}

    if (sndFile.length() != 0) {
        player.stop();
        player.loadSound(sndFile, false);
        sndFile = "";
        player.play();
    }

    ofBackground(255);
}

//--------------------------------------------------------------
void testApp::draw(){
    if (nImages > 0){
        // change no colour on the previous screen - draw it on white
        ofSetColor(ofColor::white);
        screen.draw(0, 0, scrWd, scrHt);
        if (alpha) {
            ofEnableAlphaBlending();
            ofSetColor(r, g, b, alpha);
        }

        images[currentImage].draw(active.x, active.y, active.width, active.height);
        if (alpha) {
            ofDisableAlphaBlending();
        }

        if ((currentFrame++ > 60) && !alpha) {
            reduceRect(active);
        }
        // slightly move the image
        if ((currentFrame % 10) && shake) {
            shakeRect(active);
        }

        for (int i=0; i<sizeof(ctls)/sizeof(ctls[0]); i++) {
            ctls[i].step();
        }

    } else {
        ofSetHexColor(0x999999);
        ofDrawBitmapString("no images found", 300, 80);
    }

    if (alpha) {
        screen.loadScreenData(0, 0, scrWd, scrHt);
    } else {
        screen.loadScreenData(spread, spread, scrWd - 2 * spread, scrHt - 2 * spread);
    }

//    ofSetHexColor(0xffffff);
//    ofDrawBitmapString("updated " + updated, 5, 10);
//    ofDrawBitmapString("infoFile " + infoFile, 5, 30);
//    long now = ofGetElapsedTimeMillis();
//    long dt = now - t0;
//    char buf[100];
//    sprintf(buf, "dt %4li loadfile %i camera %i info %i", dt, loadfile, camera, info);
//    ofDrawBitmapString(buf, 5, 50);
}

//--------------------------------------------------------------
void testApp::keyPressed(int key) {
    switch (key)
    {
        case 'w':
        case 'W':
            ofToggleFullscreen();
            return;
    }

    if (nImages > 0){
        if (key != 32 || !images[currentImage].loadImage("images\\deleted.jpg")) {
            currentImage++;
            currentImage %= nImages;
            currentFrame = 0;
        }
        active = getActiveArea(images[currentImage].getWidth(), images[currentImage].getHeight());
    }
}

//--------------------------------------------------------------
void testApp::keyReleased(int key){

}

//--------------------------------------------------------------
void testApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void testApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void testApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void testApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void testApp::dragEvent(ofDragInfo dragInfo){

}

//--------------------------------------------------------------
ofRectangle testApp::getActiveArea(int width, int height)
{
    ofRectangle rect;

    scrWd = ofGetWidth();
    scrHt = ofGetHeight();

    float scX = scrWd / width;
    float scY = scrHt / height;
    float sc = scX < scY ? scX : scY;
    float wd2 = width * sc;
    float ht2 = height * sc;
    if (alpha) {
        rect.x = (scrWd - wd2) / 2;
        rect.y = (scrHt - ht2) / 2;
        rect.height = ht2;
        rect.width = wd2;
    } else {
        const float RND = 30.0;
        wd2 /= 2;
        ht2 /= 2;
        float cx = (scrWd - RND)/ 2 + ofRandom(0, RND);
        float cy = (scrHt - RND) / 2 + ofRandom(0, RND);
        rect.x = cx - ht2 / 2;
        rect.y = cy - wd2 / 2;
        rect.height = ht2;
        rect.width = wd2;
    }

    return rect;
}

//--------------------------------------------------------------
void testApp::shakeRect(ofRectangle &rct)
{
    int rnd = ofRandom(-shake, shake);
    rct.x += rnd + movX;
    rnd = ofRandom(-shake, shake);
    rct.y += rnd + movY;
}

//--------------------------------------------------------------
void testApp::reduceRect(ofRectangle &rct)
{
    rct.x++;
    rct.y++;
    rct.width -= 2;
    rct.height -= 2;
}

//--------------------------------------------------------------
bool testApp::getNewImages()
{
    string fn = provider.nextImageFile();
    if (fn.length() == 0) return false;
    int p = fn.find("-a.jpg");
    if (p != string::npos) {
        updated = fn;
        newimages(updated, fn.replace(p, 6, "-b.jpg"));
        return true;
    } else {
        updated = fn;
        cout << "not expected file name "<< fn << endl;
    }
    return false;
}

//--------------------------------------------------------------
void testApp::fileMove(string f, string dd)
{
    // only move from res dir
    if (f.find(resDir) == string::npos) return;
    ofFile of(f);
    string dst = dd + "\\" + of.getFileName();
    of.close();
    bool ok = ofFile::moveFromTo(f, dst, true, false);
    cout << "move: " << f << " to " << dst << " ok=" << ok << endl;
}

//--------------------------------------------------------------
CameraWriter::CameraWriter()
{
    string ts = ofGetTimestampString("%m%d%H%M");
    strcpy(prefix, ts.c_str());
}

//--------------------------------------------------------------
bool CameraWriter::init(string ids, ImageProvider *ip)
{
    m_ip = ip;
    char buf[1000];
    strcpy(buf, ids.c_str());
    for (char *p = strtok(buf, ", "); p; p = strtok(0, ", "))
    {
        int id = atoi(p);
        if(!initGrabber(id)) return false;
    }

    grIter = grabbers.begin();
    return grabbers.size() > 0;
}

//--------------------------------------------------------------
bool CameraWriter::initGrabber(int icam)
{
    ofVideoGrabber *gr = new ofVideoGrabber();
    gr->setDeviceID(icam);
    if (!gr->initGrabber(wd, ht)) {
        return false;
    }

    ofSleepMillis(camDelay / 2);

    while(true){
        gr->update();
        if (gr->isFrameNew()) break;
        ofSleepMillis(camDelay / 3);
    }

    ofSleepMillis(camDelay / 2);
    grabbers.insert(grabbers.begin(), gr);
    return true;
}

//--------------------------------------------------------------
void CameraWriter::capture()
{
    if (isThreadRunning()) { return; }
    startThread();
}

//--------------------------------------------------------------
void CameraWriter::setPath(string &p) {
    path = p;
}

//--------------------------------------------------------------
void CameraWriter::threadedFunction()
{
    if (serial) {
        // every write turns on a LED for a duration
        // determined in Arduino program
        unsigned char b = icam;
        serial->writeBytes(&b, 1);
        // cout << "to Arduino: " << b << endl;
    }

    ofVideoGrabber *cam = *grIter;
    icam++;
    if (++grIter == grabbers.end())
    {
        grIter = grabbers.begin();
        icam = 0;
    }

    cam->update();

	ofImage img;
    img.setFromPixels(cam->getPixels(), cam->width, cam->height, OF_IMAGE_COLOR, true);
    char buf[120];
    sprintf(buf, "%s-%05i-a", prefix, cnt);
    string f = path + "\\" + buf + ".jpg";
    img.saveImage(f);
    cout << "saved " << f << endl;
    m_ip->addImageFile(f);

    imgPlayer::prepareBW(img, img, *outline);
    sprintf(buf, "%s-%05i-b", prefix, cnt++);
    f = path + "\\" + buf + ".jpg";
    img.saveImage(f);
    cout << "saved " << f << endl;
    cnt++;
    m_ip->revertTimer();
}

//--------------------------------------------------------------
void Pop3Writer::fetch()
{
    if (isThreadRunning()) { return; }
    startThread();
}

//--------------------------------------------------------------
void Pop3Writer::threadedFunction()
{
    int cnt = m_pop3.gotImages();
    if (cnt == 0) {
       cout << "no images in a mailbox, let's load local files" << endl;
   } else {
       cout << "email: " << cnt << " new images" << endl;
   }
   m_ip->revertTimer();
}

//--------------------------------------------------------------
void testApp::newimages(string a, string b)
{
    updated = a;
    imp.startConversion(b.c_str(), this, lowFreq, hiFreq);
}

//--------------------------------------------------------------
void ImageProvider::init(bool e, int p, int *s, string dir)
{
    t0 = ofGetElapsedTimeMillis();
    email = e;
    period = p;
    stock = s;
    stockDir = dir;
}

//--------------------------------------------------------------
void ImageProvider::addImageFile(string f)
{
    files.push_back(f);
}

//--------------------------------------------------------------
string ImageProvider::nextImageFile()
{
    long now = ofGetElapsedTimeMillis();
    // don't do anything too soon
	if (now - t0 < 500) return "";

	if (now - t0 > period) {
	    switch (state)
	    {
	        case Init:
                if (email)
                {
                    // the t0 shall be reverted to current time when download ends
                    t0 = now + EMAIL_TIMEOUT;
                    // if there are no images in a mailbox, the pop3writer sets loadfile flag
                    pop3Writer.fetch();
                    state = Email;
                }
                else
                {
                    state = File;
                    break;
                }
                // fall through
	        case Email:
	        case Camera:
                return ""; // nothing yet
	        break;
	    }

        if (files.size() > 0)
        {
            t0 = now;
            string f = files.front();
            files.pop_front();
            state = Init;
            return f;
        }
        else
        {
            bool st = (rand() % 100) < (*stock);
            if (st)
            {
                ofDirectory dir(stockDir);
                int cnt = dir.listDir(stockDir);
                if (!cnt) return "";
                // randomise the index if the files are used
                int imgIdx = rand() % cnt;
                string fname;
                bool found = false;
                for(int i=0; i<cnt; ++i){
                    fname = dir.getPath(imgIdx % cnt);
                    // OK if this is not the image made for sound generation
                    if (fname.find("-b.jpg") == string::npos) {
                        cout << "image: " << fname << " found in " << stockDir << " imgIdx " << imgIdx << " cnt " << cnt << endl;
                        found = true;
                        break;
                    }
                    imgIdx++;
                }
                if (!found) return "";
                addImageFile(fname);
                t0 = now;
                state = Init;
            }
            else
            {
                t0 = now + EMAIL_TIMEOUT;
                cameraWriter.capture();
                state = Camera;
            }
        }
 	}
    return "";
}

void Pop3Writer::setApp(ImageProvider *ip, testApp *a, ofxIniSettings &ini, string path, int *o) {
    m_ip = ip;
    m_pop3.init(ini);
    m_pop3.setPath(path, o, &a->provider);
}

void ValueController::step()
{
    long t = ofGetElapsedTimeMillis();
    if (t0 + dt > t) return;
    t0 = t;

    int val = *v;
    if (st > 0) {
        val += st;
        if (val >= mx) {
            val = mx;
            st = -st;
            cout << nm << " reached " << mx << endl;
        }
    } else {
        val += st;
        if (val <= mn) {
            val = mn;
            st = -st;
            cout << nm << " reached " << mn << endl;
        }
    }
    *v = val;
}

void ValueController::init(ofxIniSettings &ini, string id, int *var)
{
    mn = ini.get(id + "Mn", 0);
    mx = ini.get(id + "Mx", 127);
    int res = ini.get(id + "Res", 0);
    st = res ? (mx - mn) / res : 0;
    if (st) *var = mn;
    v = var;
    nm = id;
    t0 = ofGetElapsedTimeMillis();
    int p = ini.get(id + "Period", 0);
    dt = res ? p * 1000 / res : 10000;
    cout << nm << " step=" << st << " dt= " << dt << endl;
}
