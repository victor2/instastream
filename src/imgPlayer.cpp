#include "ofxOpenCv.h"

#include "imgPlayer.h"
#include "testApp.h"

ofxCvContourFinder imgPlayer::cf;

imgPlayer::imgPlayer() : SAMPLING(44100), BSZ(512)
{
    //ctor
    bCosWin.Init(BSZ * 2);
    bCosWin[0]=1;
    bCosWin[1]=-0.5;
    bCosWin.TFF();

}

imgPlayer::~imgPlayer()
{
    //dtor
}

void imgPlayer::writeWav(const char * filename, int nch, int cnt, short *wave)
{
	wave_head head={{'R','I','F','F'}, 0L,
	{'W','A','V','E','f','m','t',' '}, 16L,
	{
		1,	// wFormatTag
		1,	// nChannels
		SAMPLING,	// nSamplesPerSec
		SAMPLING * 2,	// nAvgBytesPerSec
		2,	// nBlockAlign
		16,	// wBitsPerSample
	},
	{'d','a','t','a'},
	0L
	};

	FILE *o = fopen(filename,"wb");
	if(!o)
	{
        cout << "Can not create " << filename << endl;
		return;
	}

	head.fmt.nSamplesPerSec = SAMPLING;
	head.fmt.wBitsPerSample = 16;
	head.fmt.nChannels = nch;
    int bytes=head.fmt.wBitsPerSample/8;
	head.len1=36 + (head.len3 = bytes*cnt);
	head.fmt.nAvgBytesPerSec = head.fmt.nSamplesPerSec*
		(head.fmt.nBlockAlign = (WORD)(head.fmt.nChannels*bytes));
	fwrite(&head, sizeof(head), 1, o);
    fwrite(wave, sizeof(short), cnt, o);
	fclose(o);
}

// allocates a new array and returns its size in cnt
short * imgPlayer::readWav(const char * filename, int &cnt)
{
    short *wave = 0;
    wave_head wh;
	FILE *o = fopen(filename,"rb");
	if(!o)
	{
        cout << "Can not read " << filename << endl;
		return 0;
	}

	if (fread(&wh, sizeof(wh), 1, o) == 1)
	{
        int bytes = wh.fmt.wBitsPerSample/8;
        cnt = wh.len3 / bytes;
        wave = new short[cnt];
        fread(wave, sizeof(short), cnt, o);
	}
	else
	{
	    cout << "Error reading " << filename << endl;
	}
	fclose(o);
	return wave;
}


void AutoScale(int cnt, short *wave, float *buff)
{
    float mn = buff[0], mx = mn;
    for (int i=1; i<cnt; i++)
    {
        float v = buff[i];
        if (v > mx) mx = v;
        if (v < mn) mn = v;
    }
    float mnn = fabs(mn);
    float mxx = fabs(mx);
    if (mnn > mxx) mxx = mnn;
    if (mxx == 0) { cout << "zeroes!" << endl;  return; }// nothing to do
    float scale = MAXSHORT / mxx;
    for (int i=0; i<cnt; i++)
    {
        wave[i] = buff[i] * scale;
    }
}

// max value should be 1.0 to avoid overflow of EXP operation later.
void imgPlayer::LoadLine(ofImage &pic, Block &blo, int x)
{
	int ht = pic.width,
	    wd = pic.height,
	    wd0 = wd,
    	nk = blo.GetSize();
    if (wd > nk) wd = nk;
    blo.Fill(0);
    unsigned char * pixs = pic.getPixels();
    int bb = pic.bpp / 8;
    for(int count=0; count<wd; ++count)
    {
        int y = wd - count - 1;
        unsigned char p = pixs[(x + ht * y) * bb];
      	blo[count] = p / 255.0;
    }
}

void imgPlayer::LoadLineRange(ofImage &pic, Block &blo, Block &tmp, int x, int kd, int kf)
{
    LoadLine(pic, tmp, x);
    blo.Fill();
    float s = (float)(tmp.GetSize() - 1) / (kf - kd);
    for (int i = kd; i <= kf; ++i) {
        int idx = s * (i - kd);
        blo[i] = tmp[idx];
    }
}

void imgPlayer::InvFourieProc(ofImage &pic, short *wave, int kd, int kf)
{
    int lineNum = pic.width,
        sz = lineNum * BSZ;

    Block
    	bModule(BSZ),	// module & phase - (0,1)
        bModuleTmp(pic.height),
        bPhase(BSZ),
        bComplex(BSZ*2), // complex spectrum -> time signal (2)
        bBufferAdd(BSZ),	// buffer for ADBB 	(3)
        bBufferPrv(BSZ);	// buffer for saved half of prev. data (4)
    float
        range = 60, // GetFloat( Form1->InvFourieRange->Text, 40 ),
	    sampling = SAMPLING, // GetFloat( Form1->InvFourieSampling->Text, 44100 ),
    	*buff= new float[sz],
    	*bf = buff,
        resolution = sampling / (BSZ * 2);

    int sz1 = sizeof(buff[0]);

    for(int count=0; count<BSZ; ++count) bPhase[count] = ofRandom(0, 360);

    ZeroMemory( buff, sizeof( buff[0] ) * sz );
    bBufferAdd.Fill();
    bBufferPrv.Fill();

    for(int cnt=0; cnt<lineNum; ++cnt)
    {
        LoadLineRange(pic, bModule, bModuleTmp, cnt, kd, kf);
//        RmaxType r;
//        bModule.FindMax(r);
//            cout << r.fMin << " " << r.fMax << " at " << r.iMax << endl;

        bModule.Mul(range).Add(-range).PowX10();

        float deltaPhi = (cnt & 1) ? 180 : -180;
        for(int count=1; count<BSZ; count+=2) bPhase[count] += deltaPhi;
        bComplex.DeModPhase( bModule, bPhase );
        bComplex.TFF();
        bComplex.Mul( bCosWin );
        bBufferAdd.CopyData( bComplex, 0, BSZ-1, 0 );
        bBufferAdd.Add( bBufferPrv );
        bBufferPrv.CopyData( bComplex, BSZ, BSZ*2-1, 0 );
        for(int j=0; j<BSZ; ++j, ++bf) *bf = bBufferAdd[j];
    }

	AutoScale(sz, wave, buff);
    delete [] buff;
}

void printType(ofImage &img) {
    switch (img.type) {
        case OF_IMAGE_GRAYSCALE:
            cout << "OF_IMAGE_GRAYSCALE" << endl;
            break;
        case OF_IMAGE_COLOR:
            cout << "OF_IMAGE_COLOR" << endl;
            break;
        case OF_IMAGE_COLOR_ALPHA:
            cout << "OF_IMAGE_COLOR_ALPHA" << endl;
            break;

    }
}

void imgPlayer::convert(ofImage &img, const char *sndFile, float low, float high)
{
    int cnt = img.width * BSZ;
    cout << " will make " << cnt << " samples -> " << ((float)cnt / SAMPLING) << " sec" << endl;
//    printType(img);
    short *wave = new short[cnt];
    memset(wave, 0, sizeof(short) * cnt);
    int kd = BSZ * low / SAMPLING;
    int kf = BSZ * high / SAMPLING;
    if (kf == 0) kf = BSZ - 1;
    InvFourieProc(img, wave, kd, kf);

    writeWav(ofToDataPath(sndFile).c_str(), 1, cnt, wave);
    delete [] wave;
}

void imgPlayer::prepareBW(ofImage &src, ofImage &dest, int outln)
{
    ofxCvColorImage			colorImg;
    colorImg.allocate(src.width, src.height);
    ofxCvGrayscaleImage 	grayImage;
    grayImage.allocate(src.width, src.height);

    src.setImageType(OF_IMAGE_COLOR);
    colorImg.setFromPixels(src.getPixels(), src.width, src.height);
    grayImage = colorImg;
    grayImage.contrastStretch();
    grayImage.threshold(128, false);
    cf.findContours(grayImage, 10, src.width * src.height  / 2, 100, true);
//    cvCanny(grayImage.getCv8BitsImage(), colorImg.getCvImage(), 100, 800, 5);
    grayImage.clear();
    grayImage.allocate(src.width, src.height);
    for (int j=0; j<cf.blobs.size(); j++)
    {
        ofxCvBlob &blob = cf.blobs[j];
        CvPoint* pts = new CvPoint[blob.nPts];
        for( int i=0; i < blob.nPts ; i++ ) {
            pts[i].x = (int)blob.pts[i].x;
            pts[i].y = (int)blob.pts[i].y;
        }
        int nPts = blob.nPts;
        cvPolyLine(grayImage.getCvImage(), &pts, &nPts, 1, 1,
                       CV_RGB(255,255,255), outln);
        delete [] pts;
    }
    colorImg = grayImage;
    dest.setFromPixels(colorImg.getPixels(), src.width, src.height, OF_IMAGE_COLOR, true);
}

void imgPlayer::startConversion(const char *imgFile, testApp *a, float low, float high)
{
    app = a;
    lowFreq = low;
    hiFreq = high;
    fname = imgFile;
    startThread();
}

void imgPlayer::threadedFunction()
{
    ofImage img;
    if (img.loadImage(fname))
    {
        const char * s = "snd.wav";
        convert(img, s, lowFreq, hiFreq);
        app->newSound(s, fname.c_str());
    }
}
