#include "Pop3Client.h"
#include "testApp.h"
//#include "imgPlayer.h"
//#include "ofxIniSettings.h"
#include "Poco/Net/NetException.h"
#include "Poco/Net/PartHandler.h"
#include "Poco/Net/DNS.h"

using Poco::Net::POP3ClientSession;
using Poco::Net::MessageHeader;
using Poco::Net::MailMessage;
using Poco::Net::PartHandler;
using Poco::Net::POP3Exception;
using Poco::Net::DNS;
using Poco::Net::HostEntry;
using Poco::Net::IPAddress;

static int failures = 0;

class ImagePartHandler : public PartHandler {
    int cnt;
    int outline;
    string fn;
    string fna;
    string fnb;
    ImageProvider *ip;
public:
    ImagePartHandler(string fname, int outl, ImageProvider *a) {
        cnt = 0;
        outline = outl;
        fn = fname;
        ip = a;
    }

    void handlePart(const MessageHeader& header, std::istream& stream)
    {
        if (!header.has("Content-Type")) return;
        string ctype = header["Content-Type"];
        // it's an image
        if (ctype.find("image") != string::npos) {
            ofstream fstr;
            char ch;
            fna = fn + "--a.jpg";
            fstr.open(ofToDataPath(fna).c_str(), ios_base::binary);
            fstr << stream.rdbuf();

            fstr.close();
            ofImage img;
            if (img.loadImage(fna)) {
                imgPlayer::prepareBW(img, img, outline);
                fnb = fn + "--b.jpg";
                img.saveImage(fnb);
                cout << "saved " << fn << endl;
                ip->addImageFile(fna);
                ++cnt;
            } else {
                cout << "failed to read " << fna << endl;
            }
        }
    }

    int getCounter() {
        return cnt;
    }
};

Pop3Client::Pop3Client()
{
    //ctor
    m_Port = 0;
    string ts = ofGetTimestampString("%m%d%H%M");
    strcpy(m_Prefix, ts.c_str());
}

Pop3Client::~Pop3Client()
{
    //dtor
}

bool hasNetwork(bool verb)
{
    HostEntry he = DNS::thisHost();
    IPAddress ad = DNS::resolveOne(he.name());
    if (verb)
    {
        cout << he.name() << " " << ad.toString() << endl;
    }
    return !ad.isLoopback();
}

void Pop3Client::init(ofxIniSettings &ini) // returns the number
{
    if (ini.get("email",string("false")) == "true") {
        init(ini.get("emailhost", string("")),
             ini.get("emailport", 0),
             ini.get("emailuser", string("")),
             ini.get("emailpass", string("")),
             ini.get("emailword", string("")) );

        if (!hasNetwork(true))
        {
            cout << "No network - disabling email" << endl;
            m_Port = 0;
        }
    }
}

int Pop3Client::gotImages() // returns the number
{
    if (m_Port == 0 || m_Host.length() == 0 || !hasNetwork(false)) return 0;
    cout << "fetching email" << endl;
    int cnt = 0;
    try {
        char buf[120];
        char subject[4096];
        subject[sizeof(subject) - 1] = 0;
        POP3ClientSession session(m_Host, m_Port);
        try
        {
            session.login(m_User, m_Pass);
            std::vector<POP3ClientSession::MessageInfo> infos;
            session.listMessages(infos);
            cout << "POP 3 login OK " << infos.size() << " messages " << endl;
            for (int i=0; i<infos.size(); ++i) {
                MessageHeader header;
                session.retrieveHeader(infos[i].id, header);
                if (!header.has("Subject")) continue;
                strncpy(subject, header["Subject"].c_str(), sizeof(subject) - 1);
                for (char *c = subject; *c ; ++c) {
                    *c = tolower( *c );
                }
                //# only letters with this word in subject are processed
                // emailword=image
                if (!strstr(subject, m_Subject.c_str())) continue;

                sprintf(buf, "%s-%05i", m_Prefix, m_Cnt++);
                string f = m_Path + "\\" + buf;
                ImagePartHandler parts(f, *m_pOutline, m_Ip);
                // cout << "msg " << infos[i].id << " " << f << endl;
                MailMessage message;
                session.retrieveMessage(infos[i].id, message, parts);
                cnt += parts.getCounter();
                if (header.has("From")) {
                    ofstream of("addr.log", ofstream::app);
                    of << header["From"] << endl;
                }
             	session.deleteMessage(infos[i].id);
            }
        }
        catch (POP3Exception& e)
        {
            cout << "login failed, email disabled " << e.name() << " " << e.what()
                << " host=" << m_Host << ":" << m_Port << endl;
            m_Port = 0;
        }
        session.close();

    } catch (Poco::Exception &e) {
        ++failures;
        cout << "Poco exception " << e.name() << " " << e.what()
            << " host=" << m_Host << ":" << m_Port
            << " failed " << failures << endl;
        if (failures >= 10) {
            m_Port = 0;
            cout << "email disabled due to failures" << endl;
        }
    }
    return cnt;
}

/*


void POP3ClientSessionTest::testRetrieveMessage()
{
	std::string cmd = server.popCommand();
	assert (cmd == "RETR 1");

	assert (message.getContent() ==
		"Hello Jane,\r\n"
		"\r\n"
		"blah blah blah...\r\n"
		"...\r\n"
		"\r\n"
		"Yours, John\r\n"
	);

	session.close();
}


void POP3ClientSessionTest::testRetrieveMessages()
{
	DialogServer server;
	server.addResponse("+OK POP3 Ready...");
	server.addResponse("+OK USER");
	server.addResponse("+OK PASS");
	server.addResponse(
		"+OK Here comes the message\r\n"
		"From: john.doe@no.where\r\n"
		"To: jane.doe@no.where\r\n"
		"Subject: test\r\n"
		"\r\n"
		"."
	);
	server.addResponse(
		"+OK Here comes the message\r\n"
		"From: john.doe@no.where\r\n"
		"To: jane.doe@no.where\r\n"
		"Subject: test\r\n"
		"\r\n"
		"Hello Jane,\r\n"
		"\r\n"
		"blah blah blah...\r\n"
		"....\r\n"
		"\r\n"
		"Yours, John\r\n"
		"."
	);
	server.addResponse("+OK QUIT");
	POP3ClientSession session("localhost", server.port());
	session.login("user", "secret");
	server.clearCommands();
	MessageHeader header;
	session.retrieveHeader(1, header);
	std::string cmd = server.popCommand();
	assert (cmd == "TOP 1 0");
	assert (header.get("From") == "john.doe@no.where");
	assert (header.get("To") == "jane.doe@no.where");
	assert (header.get("Subject") == "test");

	MailMessage message;
	session.retrieveMessage(2, message);
	cmd = server.popCommand();
	assert (cmd == "RETR 2");

	assert (message.getContent() ==
		"Hello Jane,\r\n"
		"\r\n"
		"blah blah blah...\r\n"
		"...\r\n"
		"\r\n"
		"Yours, John\r\n"
	);
	session.close();
}


void POP3ClientSessionTest::testDeleteMessage()
{
	DialogServer server;
	server.addResponse("+OK POP3 Ready...");
	server.addResponse("+OK USER");
	server.addResponse("+OK PASS");
	server.addResponse("+OK DELETED");
	server.addResponse("+OK QUIT");
	POP3ClientSession session("localhost", server.port());
	session.login("user", "secret");
	server.clearCommands();
	session.deleteMessage(42);
	std::string cmd = server.popCommand();
	assert (cmd == "DELE 42");
	session.close();
}
*/
